from email import message
import email
from itertools import product
from multiprocessing import context
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render,redirect
from store_app.models import Product,Categories,FILTER_PRICE,Color,Brand,contact_us
from django.contrib.auth.models import User
from django.contrib.auth import authenticate ,login,logout
from django.contrib.auth.decorators import login_required
from cart.cart import Cart


def Base(request):
    return render(request,'main/base.html')

def Home(request):
    product=Product.objects.filter(status="Publish")
    context={
        'product':product, 
    }
    return render(request,'main/index.html',context)


def Products(request):
    product=Product.objects.filter(status="Publish")
    categories=Categories.objects.all()
    filter_price=FILTER_PRICE.objects.all()
    color=Color.objects.all()
    brand=Brand.objects.all()


    
    CATID=request.GET.get('categories')
    FILTER_PRICE_ID=request.GET.get('filter_price')
    COLOR_ID=request.GET.get('color')
    BRAND_ID=request.GET.get('brand')

    ATOZID=request.GET.get('ATOZ')
    ZTOAID=request.GET.get('ZTOA')
    LOWTOHIGHID=request.GET.get('LOWTOHIGH')
    HIGHTOLOWID=request.GET.get('HIGHTOLOW')
    NEW_PRODUCTID=request.GET.get('NEWPRODUCT')
    OLD_PRODUCTID=request.GET.get('OLDPRODUCT')

    

    if CATID:
        product=Product.objects.filter(Categories=CATID,status="Publish")
    
    elif FILTER_PRICE_ID:
        product=Product.objects.filter(filter_price=FILTER_PRICE_ID,status="Publish")

    elif COLOR_ID:
        product=Product.objects.filter(color=COLOR_ID,status="Publish")

    elif BRAND_ID:
        product=Product.objects.filter(brand=BRAND_ID)

    elif ATOZID:
        product=Product.objects.filter(status="Publish").order_by('name')

    elif ZTOAID:
        product=Product.objects.filter(status='Publish').order_by('-name')

    elif LOWTOHIGHID:
        product=Product.objects.filter(status='Publish').order_by('price')
    
    elif HIGHTOLOWID:
        product=Product.objects.filter(status='Publish').order_by('-price')

    elif NEW_PRODUCTID:
        product=Product.objects.filter(status='Publish',condition='New')

    elif OLD_PRODUCTID:
        product=Product.objects.filter(status='Publish',condition='Old')


    else:
        product=Product.objects.filter(status="Publish")

    
    
    context={
        'product':product, 
        'categories':categories,
        'f_price':filter_price,
        'color':color,
        'brand':brand
    }

    return render(request,'main/product.html',context)



def Search(request):
    query=request.GET.get('query')
    product=Product.objects.filter(name__icontains=query)

    context={
        'product':product
    }

    return render(request,'main/search.html',context)

def Product_details_page(request,id):
    prod=Product.objects.filter(id = id).first()
    context={
        'prod':prod,
    }
    return render(request,'main/product_single.html',context)

def CONTACT(request):
    if request.method == "POST":
        name=request.POST.get('name')
        email=request.POST.get('email')
        subject=request.POST.get('subject')
        message=request.POST.get('message')

        contact=contact_us(
            name=name,
            email=email,
            subject=subject,
            message=message,
        )

        subject=subject
        message=message
        email_from=settings.EMAIL_HOST_USER
        send_mail(subject,message,email_from,['maulikg0987@gmail.com'])
        contact.save()
        return redirect('home') 


    return render(request,'main/contact.html')


def HandleRegister(request):
    if request.method == 'POST':
        username=request.POST.get('username')
        first_name=request.POST.get('first_name')
        last_name=request.POST.get('last_name')
        email=request.POST.get('email')
        pass1=request.POST.get('pass1')
        pass2=request.POST.get('pass2')


        customer=User.objects.create_user(username,email,pass1)
        customer.first_name=first_name
        customer.last_name=last_name
        customer.save()


        return redirect('register')

    return render(request,'registration/auth.html')


def Handlelogin(request):
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')

        user=authenticate(username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else: 
            return redirect('login')
       
            
    return render(request,'registration/auth.html')

def Handlelogout(request):
    logout(request)
    
    return redirect('home')


# cart

@login_required(login_url="/login/")
def cart_add(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("home")


@login_required(login_url="/login/")
def item_clear(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.remove(product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def item_increment(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def item_decrement(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.decrement(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login/")
def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect("cart_detail")


@login_required(login_url="/login/")
def cart_detail(request):
    return render(request, 'cart/cartdetails.html')


def Cheak_out(request):
    return render(request,'cart/cheakout.html')

def place_order(request):
    
    return render(request,'cart/placeorder.html')