

from unicodedata import name
from xml.dom.minidom import Document
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views



urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.Home,name="home"),
    path('base/',views.Base,name="base"),
    path('product/',views.Products,name="product"),
    
    path('search/',views.Search, name='search'),
    path('product_details/<str:id>',views.Product_details_page,name='product_detail'),
    path('contact/',views.CONTACT,name='contact'),
    
    path('register/',views.HandleRegister,name='register'),
    path('login/',views.Handlelogin,name='login'),
    path('logout/',views.Handlelogout,name='logout'),
   
    # cart
    path('cart/add/<int:id>/', views.cart_add, name='cart_add'),
    path('cart/item_clear/<int:id>/', views.item_clear, name='item_clear'),
    path('cart/item_increment/<int:id>/',views.item_increment, name='item_increment'),
    path('cart/item_decrement/<int:id>/', views.item_decrement, name='item_decrement'),
    path('cart/cart_clear/', views.cart_clear, name='cart_clear'),
    path('cart/cart-detail/',views.cart_detail,name='cart_detail'),
    path('cart/cheakout/',views.Cheak_out,name='cheakout'),
    path('cart/cheakout/placeorder/',views.place_order,name='placeorder')




]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
